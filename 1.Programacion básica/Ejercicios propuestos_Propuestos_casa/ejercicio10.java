//Crea una variable de tipo "Pattern" que permita realizar operaciones con expresiones regulares.
import java.util.regex.Pattern;
public class ejercicio10{
  public static void main(String[] args) {
    Pattern pattern = Pattern.compile("\\d+");
    String texto = "La edad de Yeick es 19 y la de Gimena es 17";
    String[] numeros = pattern.split(texto);
    for (String numero : numeros) {
      System.out.println(numero);
    }
  }
}