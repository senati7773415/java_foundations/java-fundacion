import java.text.DateFormat;
import java.util.Date;
public class ejercicio12 {
  public static void main(String[] args) {
    Date fecha = new Date();
    DateFormat formatoFecha = DateFormat.getDateInstance(DateFormat.SHORT);
    DateFormat formatoHora = DateFormat.getTimeInstance(DateFormat.MEDIUM);
    System.out.println("Fecha : " + formatoFecha.format(fecha));
    System.out.println("Hora : " + formatoHora.format(fecha));
  }
}