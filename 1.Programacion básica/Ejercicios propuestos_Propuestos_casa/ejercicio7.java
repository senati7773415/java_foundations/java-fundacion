import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
public class ejercicio7 {
  public static void main(String[] args) {
    try {
      BufferedReader reader = new BufferedReader(new FileReader("trabajo/all/archivo.txt"));
      String linea = reader.readLine();
      while (linea != null) {
        System.out.println(linea);
        linea = reader.readLine();
      }
      reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}