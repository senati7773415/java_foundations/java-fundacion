//Crea una variable de tipo "SimpleDateFormat" que permita dar formato a fechas y horas personalizado.
import java.text.SimpleDateFormat;
import java.util.Date;
public class ejercicio13 {
  public static void main(String[] args) {
    Date fecha = new Date();
    SimpleDateFormat formatoFecha = new SimpleDateFormat("DD/MM/yyyy");
    SimpleDateFormat formatoHora = new SimpleDateFormat("HH:MM:SS");
    System.out.println("Fecha formateada: " + formatoFecha.format(fecha));
    System.out.println("Hora formateada: " + formatoHora.format(fecha));
  }
}