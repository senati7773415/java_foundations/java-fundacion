import java.util.HashMap;
public class ejercicio2 {
    public static void main(String[] args) {
        HashMap<String, Integer> personas = new HashMap<String, Integer>();
        personas.put("Jose", 19);
        personas.put("Cielo", 16);
        personas.put("Wilmer", 17);
        personas.put("Gimena", 20);
        int edadDeJose = personas.get("Jose");
        System.out.println("La edad de Jose es " + edadDeJose);
        for (String nombre : personas.keySet()) {
            int edad = personas.get(nombre);
            System.out.println(nombre + " tiene " + edad + " años.");
        }
    }
}

