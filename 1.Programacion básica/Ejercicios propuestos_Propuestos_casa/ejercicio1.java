//Crea una variable de tipo "ArrayList" que contenga una lista de nombres de personas.
import java.util.ArrayList;
public class ejercicio1 {
    public static void main(String[] args) {
        ArrayList<String> nombres = new ArrayList<String>();
        nombres.add("Jose");
        nombres.add("Cielo");
        nombres.add("Wilmer");
        nombres.add("Gimena");
        for (String Nombre : nombres) {
            System.out.println(Nombre);
        }
    }
}
