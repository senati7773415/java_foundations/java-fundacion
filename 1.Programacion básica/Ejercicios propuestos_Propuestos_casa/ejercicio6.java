//Crea una variable de tipo "Scanner" que permita leer la entrada del usuario por consola.
import java.util.Scanner;

public class ejercicio6 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Introduzca un número: ");
    int numero = scanner.nextInt();
    System.out.println("El número introducido es: " + numero);
  }
}