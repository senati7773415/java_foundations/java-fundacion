//Crea una variable de tipo "BigDecimal" que permita realizar operaciones matemáticas con números de alta precisión.
import java.math.BigDecimal;
public class ejercicio14 {
  public static void main(String[] args) {
    BigDecimal numero1 = new BigDecimal("10.5");
    BigDecimal numero2 = new BigDecimal("15.7");
    BigDecimal resultado = numero1.add(numero2);
    System.out.println("La respuesta del resultado es: " + resultado);
  }
}