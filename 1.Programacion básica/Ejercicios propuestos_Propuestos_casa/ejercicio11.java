import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class ejercicio11 {
  public static void main(String[] args) {
    Pattern pattern = Pattern.compile("\\d+");
    String texto = "La edad de Yeickn es 19 y la de Gimena es 17";
    Matcher matcher = pattern.matcher(texto);
    while (matcher.find()) {
      System.out.println("Número encontrado: " + matcher.group());
    }
  }
}