import java.util.Scanner;

public class Ejercicio13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduzca una palabra o una frase: ");
        String entrada = scanner.nextLine();
        String[] palabras = entrada.split(" ");
        int contador = 0;
        for (String palabra : palabras) {
            if (tieneLetraRepetidaEntre3y4Veces(palabra)) {
                contador++;
            }
        }
        System.out.println("Son " + contador + " palabras con una letra que aparece más de 2 veces pero menos de 5 veces.");
    }
    
    public static boolean tieneLetraRepetidaEntre3y4Veces(String palabra) {
        for (char c = 'b'; c <= 'e'; c++) {
            int contador = 0;
            for (int i = 0; i < palabra.length(); i++) {
                if (palabra.charAt(i) == c) {
                    contador++;
                }
                if (contador == 5) {
                    break;
                }
            }
            if (contador > 2 && contador < 5) {
                return true;
            }
        }
        return false;
    }
}
