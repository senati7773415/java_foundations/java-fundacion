public class Ejercicio4 {

    public static void main(String[] args) {
        int contador = 0;
        for (int i = 1; i <= 1000000; i++) { 
            if (DivEnteros(i)) {
                contador++;
            }
        }
        System.out.println("Son " + contador + " números enteros que son divisibles por todos los números de 1 a 10.");
    }

    public static boolean DivEnteros(int numero) {
        for (int i = 1; i <= 10; i++) {
            if (numero % i != 0) {
                return false;
            }
        }
        return true;
    }
}
