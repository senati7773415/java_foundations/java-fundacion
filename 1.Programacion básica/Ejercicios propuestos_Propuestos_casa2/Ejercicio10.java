public class Ejercicio10 {
    public static void main(String[] args) {
        int contador = 0;
        for (int i = 1; i < 1000000; i++) {
            if (esSumaFactoriales(i)) {
                contador++;
            }
        }
        System.out.println("Son " + contador + " números, que la propiedad de ser iguales a la suma de los dígitos factoriales de su descomposición en dígitos.");
    }
    public static boolean esSumaFactoriales(int numero) {
        int sumaFactoriales = 0;
        int n = numero;
        while (n > 0) {
            int digito = n % 10;
            sumaFactoriales += factorial(digito);
            n /= 10;
        }
        return numero == sumaFactoriales;
    }
    public static int factorial(int n) {
        int resultado = 1;
        for (int i = 2; i <= n; i++) {
            resultado *= i;
        }
        return resultado;
    }
}

