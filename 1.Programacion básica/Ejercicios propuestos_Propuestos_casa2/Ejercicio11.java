import java.util.Scanner;

public class Ejercicio11 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca una frase: ");
        String frase = sc.nextLine();
        int contador = 0;
        String[] palabras = frase.split(" ");
        for (String palabra : palabras) {
            if (palabra.length() >= 4 && palabra.charAt(0) == palabra.charAt(3)) {
                contador++;
            }
        }
        System.out.println("Son " + contador + " palabras que tienen la misma letra en las posiciones 1 y 4.");
        sc.close();
    }

}
