import java.util.HashSet;
public class Ejercicio1 {
    public static void main(String[] args) {
        int[] numeros = {1, 2, 3, 4, 5}; 
        int objetivo = 7; 
        if (existeParConSuma(numeros, objetivo)) {
            System.out.println("hay un par de números que suman " + objetivo + ".");
        } else {
            System.out.println("No hay ningun par de números en la lista que sumen " + objetivo + ".");
        }
    }
    public static boolean existeParConSuma(int[] numeros, int objetivo) {
        HashSet<Integer> complementos = new HashSet<Integer>();
        for (int numero : numeros) {
            if (complementos.contains(numero)) {
                return true;
            }
            complementos.add(objetivo - numero);
        }
        return false;
    }
}
