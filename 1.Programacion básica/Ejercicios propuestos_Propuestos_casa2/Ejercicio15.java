import java.util.Scanner;
public class Ejercicio15 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduzaca una frase: ");
        String frase = scanner.nextLine();
        int contador = 0;
        String[] palabras = frase.split(" ");
        for (String palabra : palabras) {
            if (palabra.length() > 0 && palabra.charAt(0) == palabra.charAt(palabra.length() - 1)) {
                contador++;
            }
        }
        System.out.println("Son " + contador + " palabras que una misma letra al principio y al final.");
    }
}
