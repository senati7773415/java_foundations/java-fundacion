import java.util.Scanner;

public class Ejercicio7{

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduza una oración: ");
        String oracion = scanner.nextLine();
        String[] palabras = oracion.split("\\s+");
        int contador = 0;
        for (String palabra : palabras) {
            if (tieneRepetidasTresVeces(palabra)) {
                contador++;
            }
        }
        System.out.println("la oracion esta conformada" + contador + " palabras con la misma letra repetida tres veces consecutivas.");
    }

    public static boolean tieneRepetidasTresVeces(String palabra) {
        for (int i = 0; i <= palabra.length() - 3; i++) {
            if (palabra.charAt(i) == palabra.charAt(i+1) && palabra.charAt(i+1) == palabra.charAt(i+2)) {
                return true;
            }
        }
        return false;
    }
}
