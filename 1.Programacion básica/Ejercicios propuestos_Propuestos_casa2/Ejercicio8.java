public class Ejercicio8 {
    public static void main(String[] args) {
        int sumaDigitos = 0;
        for (int i = 1; i <= 100; i++) {
            sumaDigitos += sumarDigitos(i);
        }
        System.out.println("La suma de los dígitos de los números de 1 al 100 es: " + sumaDigitos);
        int numero = 1;
        while (numero <= sumaDigitos) {
            if (sumaDigitos % numero == 0 && esDivisiblePorSuma(numero)) {
                System.out.println("El número " + numero + " es divisible por la suma de los dígitos del numero de 1 al 100.");
                break;
            }
            numero++;
        }
    }
    public static boolean esDivisiblePorSuma(int numero) {
        int sumaDigitos = 0;
        for (int i = 1; i <= 100; i++) {
            sumaDigitos += sumarDigitos(i);
        }
        return numero % sumaDigitos == 0;
    }
    public static int sumarDigitos(int numero) {
        int suma = 0;
        while (numero != 0) {
            suma += numero % 10;
            numero /= 10;
        }
        return suma;
    }
}
