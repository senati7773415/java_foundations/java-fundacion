import java.util.Scanner;
public class Ejercicio14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduzca un número: ");
        int n = scanner.nextInt();
        int contador = 0;
        for (int i = 1; i <= n; i++) {
            if (esNumeroKaprekar(i)) {
                contador++;
            }
        }
        System.out.println("Son " + contador + " números de Kaprekar menor o igual a " + n);
    }
    public static boolean esNumeroKaprekar(int num) {
        int cuadrado = num * num;
        int numDigitos = contarDigitos(cuadrado);
        for (int i = 1; i < numDigitos; i++) {
            int parte1 = cuadrado / (int) Math.pow(10, i);
            int parte2 = cuadrado % (int) Math.pow(10, i);
            if (parte2 != 0 && parte1 + parte2 == num) {
                return true;
            }
        }
        return false;
    }
    public static int contarDigitos(int num) {
        int contador = 0;
        while (num != 0) {
            num /= 10;
            contador++;
        }
        return contador;
    }
}
