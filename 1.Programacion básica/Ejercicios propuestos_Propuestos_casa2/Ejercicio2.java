public class Ejercicio2 {
    public static void main(String[] args) {
        String[] palabras = {"Wilmer", "Java", "Programar", "laptop", "letras",}; 
        int contador = 0;
        for (String palabra : palabras) {
            if (VocalesConse(palabra)) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " palabras que tienen más de dos vocales .");
    }
    public static boolean VocalesConse(String palabra) {
        int contador = 0;
        for (int i = 0; i < palabra.length(); i++) {
            char letra = Character.toLowerCase(palabra.charAt(i));
            if (letra == 'A' || letra == 'E' || letra == 'I' || letra == 'O' || letra == 'U') {
                contador++;
                if (contador >= 3) {
                    return true;
                }
            } else {
                contador = 0;
            }
        }
        return false;
    }
}
