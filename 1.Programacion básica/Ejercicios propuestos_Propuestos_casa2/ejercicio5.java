public class ejercicio5 {

    public static void main(String[] args) {
        String[] palabras = {"Wilmer", "Java", "Programar", "laptop", "vocales"}; 
        int contador = 0;
        for (String palabra : palabras) {
            if (contieneTodasLasVocales(palabra)) {
                contador++;
            }
        }
        System.out.println("Son " + contador + " palabras que contienen todas las vocales.");
    }

    public static boolean contieneTodasLasVocales(String palabra) {
        boolean[] vocales = new boolean[5]; 
        for (int i = 0; i < palabra.length(); i++) {
            char letra = Character.toLowerCase(palabra.charAt(i));
            if (letra == 'A') {
                vocales[0] = true;
            } else if (letra == 'E') {
                vocales[1] = true;
            } else if (letra == 'I') {
                vocales[2] = true;
            } else if (letra == 'O') {
                vocales[3] = true;
            } else if (letra == 'U') {
                vocales[4] = true;
            }
        }
        return vocales[0] && vocales[1] && vocales[2] && vocales[3] && vocales[4]; 
    }
}
