public class Ejercicio12 {
    public static void main(String[] args) {
        int n = 1000000;
        int contador = 0;
        for (int i = 1; i <= n; i++) {
            if (esNumeroDeSmith(i)) {
                contador++;
            }
        }
        System.out.println("Son" + contador + " números de Smith menor o igual a " + n);
    }
    public static boolean esNumeroDeSmith(int n) {
        int sumaDigitos = sumaDigitos(n);
        int sumaFactoresPrimos = sumaFactoresPrimos(n);
        return sumaDigitos == sumaFactoresPrimos;
    }
    public static int sumaDigitos(int n) {
        int suma = 0;
        while (n != 0) {
            suma += n % 10;
            n /= 10;
        }
        return suma;
    }
    public static int sumaFactoresPrimos(int n) {
        int suma = 0;
        int factor = 2;
        while (n > 1) {
            while (n % factor == 0) {
                suma += sumaDigitos(factor);
                n /= factor;
            }
            factor++;
            if (factor * factor > n) {
                if (n > 1) {
                    suma += sumaDigitos(n);
                }
                break;
            }
        }
        return suma;
    }
}
