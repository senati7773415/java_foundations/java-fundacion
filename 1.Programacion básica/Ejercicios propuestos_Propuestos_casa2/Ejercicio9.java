public class Ejercicio9{
    public static void main(String[] args) {
        String[] palabras = {"Hi", "adios", "Programar", "java", "laptop", "codigo"};
        int contador = 0;
        for (String palabra : palabras) {
            if (tieneLetraRepetidaImparVeces(palabra)) {
                contador++;
            }
        }
        System.out.println("Son " + contador + " palabras que tienen una letra repetida un número impar de veces.");
    }
    public static boolean tieneLetraRepetidaImparVeces(String palabra) {
        for (int i = 0; i < palabra.length(); i++) {
            char letra = palabra.charAt(i);
            int contador = 0;
            for (int j = 0; j < palabra.length(); j++) {
                if (palabra.charAt(j) == letra) {
                    contador++;
                }
            }
            if (contador % 2 != 0) {
                return true;
            }
        }
        return false;
    }
}
