public class Ejercicio6 {

    public static void main(String[] args) {
        long numero = 2520; 
        while (!DivTod(numero)) {
            numero += 2520; 
        }
        System.out.println("El número " + numero + " es divisible por todos los números de 1 a 20.");
    }

    public static boolean DivTod(long numero) {
        for (int i = 1; i <= 20; i++) {
            if (numero % i != 0) {
                return false;
            }
        }
        return true;
    }
}
