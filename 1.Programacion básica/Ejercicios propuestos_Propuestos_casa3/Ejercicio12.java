import java.util.Scanner;
public class Ejercicio12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca un número entero de 5 cifras: ");
        int numero = sc.nextInt();
        int cifra5 = numero % 10;
        int cifra4 = (numero / 10) % 10;
        int cifra3 = (numero / 100) % 10;
        int cifra2 = (numero / 1000) % 10;
        int cifra1 = numero / 10000;
        System.out.println("Cifras del número son: " + cifra5 + ", " + cifra4 + ", " + cifra3 + ", " + cifra2 + " y " + cifra1);
    }
}