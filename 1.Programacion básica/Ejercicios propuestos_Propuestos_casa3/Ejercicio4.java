import java.util.Scanner;
public class Ejercicio4{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca una temperatura en grados centígrados: ");
        double celsius = sc.nextDouble();
        double fahrenheit = 32 + (9 * celsius / 5);
        System.out.println(celsius + " grados centígrados son " + fahrenheit + " grados Fahrenheit");
    }
}