import java.util.Scanner;
public class Ejercicio15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca un número entero: ");
        int n = sc.nextInt();
        System.out.print("Introduzca el número de cifras a quitar: ");
        int m = sc.nextInt();
        int resultado = n / (int) Math.pow(10, m);
        System.out.println("La respuesta es: " + resultado);
    }
}