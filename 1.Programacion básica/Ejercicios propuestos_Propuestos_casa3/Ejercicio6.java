import java.util.Scanner;
public class Ejercicio6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca una velocidad en Km/h: ");
        double kmh = sc.nextDouble();
        double ms = kmh * 1000 / 3600;
        System.out.println(kmh + " Km/h son " + ms + " m/s");
    }
}