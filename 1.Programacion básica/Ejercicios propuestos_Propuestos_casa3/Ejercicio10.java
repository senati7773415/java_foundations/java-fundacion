import java.util.Scanner;
public class Ejercicio10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca un número de 3 cifras: ");
        int numero = sc.nextInt();
        int cifra1 = numero / 100;
        int cifra2 = (numero / 10) % 10;
        int cifra3 = numero % 10;
        System.out.println("Las cifras del número son: " + cifra1 + ", " + cifra2 + " y " + cifra3);
    }
}