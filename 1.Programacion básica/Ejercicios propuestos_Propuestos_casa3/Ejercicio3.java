import java.util.Scanner;
public class Ejercicio3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca un número entero: ");
        int num = sc.nextInt();
        System.out.println("Número introducido: " + num);
        System.out.println("Doble de " + num + ": " + (2*num));
        System.out.println("Triple de " + num + ": " + (3*num));
        System.out.println("Cuadruple de " + num + ": " + (4*num));
    }
}