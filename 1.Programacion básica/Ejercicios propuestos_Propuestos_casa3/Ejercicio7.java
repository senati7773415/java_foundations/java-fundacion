import java.util.Scanner;
public class Ejercicio7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca el cateto opuesto: ");
        double cateto1 = sc.nextDouble();
        System.out.print("Introduzca cateto adyacente: ");
        double cateto2 = sc.nextDouble();
        double hipotenusa = Math.sqrt(Math.pow(cateto1, 2) + Math.pow(cateto2, 2));
        System.out.println("La longitud de la hipotenusa es: " + hipotenusa);
    }
}
