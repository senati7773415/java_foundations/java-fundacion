import java.util.Scanner;
public class Ejercicio11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca un número entero de 5 cifras: ");
        int numero = sc.nextInt();
        int cifra1 = numero / 10000;
        int cifra2 = (numero / 1000) % 10;
        int cifra3 = (numero / 100) % 10;
        int cifra4 = (numero / 10) % 10;
        int cifra5 = numero % 10;
        System.out.println("Las cifras del número son: " + cifra1 + ", " + cifra2 + ", " + cifra3 + ", " + cifra4 + " y " + cifra5);
    }
}