import java.util.Scanner;
public class Ejercicio13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca tu fecha de nacimiento (en formato DD-MM-YYYY): ");
        String fecha = sc.next();
        int suma = 0;
        for (int i = 0; i < fecha.length(); i++) {
            suma += Integer.parseInt(String.valueOf(fecha.charAt(i)));
        }
        while (suma > 9) {
            int aux = 0;
            while (suma > 0) {
                aux += suma % 10;
                suma /= 10;
            }
            suma = aux;
        }
        System.out.println("Tu número de la suerte siempre sera este: " + suma);
    }
}