import java.util.Scanner;
public class Ejercicio1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca el 1° número: ");
        int num1 = sc.nextInt();
        System.out.print("Introduzca el 2° número: ");
        int num2 = sc.nextInt();
        System.out.println("Los números introducidos son: " + num1 + " y " + num2);
    }
}