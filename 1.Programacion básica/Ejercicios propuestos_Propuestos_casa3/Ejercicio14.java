import java.util.Scanner;
public class Ejercicio14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca el precio por unidad (sin IVA) del producto: ");
        double preUnidad = sc.nextDouble();
        System.out.print("Introduzca el número de productos vendidos: ");
        int cantidad = sc.nextInt();
        double preSinIVA = preUnidad * cantidad;
        double IVA = 0.21;
        double preConIVA = preSinIVA * (1 + IVA);
        System.out.println("El precio final de venta del producto es: " + preConIVA);
    }
}