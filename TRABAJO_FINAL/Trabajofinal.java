import java.util.Scanner;
public class Trabajofinal {
    public static void main(String[] args) {
        String[] dni = {"74583532", "01211305", "14874589", "98745696", "25896347"};
        String[] password = {"4321", "1914", "2468", "7458", "3531"};
        String[] nombres = {"Wilmer Curro", "Fernando Gutierrez", "Evelin Mamani", "Ana Cuevas", "Gimmena Quispe"};
        double[] saldo = {1000.0, 500.0, 2000.0, 1500.0, 3000.0};
        int intentosRestantes = 3;
        boolean loggedIn = false;
        Scanner scanner = new Scanner(System.in);

        System.out.println(" NEW Perú Bank");

        while (!loggedIn && intentosRestantes > 0) {
            System.out.print("Puede introducir los datos de su DNI: ");
            String inputDNI = scanner.nextLine();
            System.out.print("Puede ingresar su clave: ");
            String inputPassword = scanner.nextLine();

            for (int i = 0; i < dni.length; i++) {
                if (inputDNI.equals(dni[i]) && inputPassword.equals(password[i])) {
                    System.out.println("Bienvenido " + nombres[i]);
                    loggedIn = true;
                    mostrarMenu(saldo[i], scanner);
                    break;
                }
            }

            if (!loggedIn) {
                intentosRestantes--;
                System.out.println("DNI o contraseña incorrectos. Le quedan " + intentosRestantes + " intentos");
            }
        }

        if (!loggedIn) {
            System.out.println("Demasiados intentos incorrectos. se cerrará en unos momentos si sigue fallando...");
        }
    }

    public static void mostrarMenu(double saldo, Scanner scanner) {
        boolean salir = false;

        while (!salir) {
            System.out.println("Elija la operación que va realizar:");
            System.out.println("1 Depósito");
            System.out.println("2 Retiro");
            System.out.println("3 Consulta de saldo");
            System.out.println("4 Salir");

            int opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    System.out.print("Ingrese el monto que va depositar: ");
                    double montoDeposito = scanner.nextDouble();
                    saldo += montoDeposito;
                    System.out.println("El monto que se va depositar es: " + montoDeposito);
                    break;
                case 2:
                    System.out.print("Introduzca el monto a retirar: ");
                    double montoRetiro = scanner.nextDouble();
                    if (montoRetiro <= saldo) {
                        saldo -= montoRetiro;
                        System.out.println("El monto que desea retirar es: " + montoRetiro);
                    } else {
                        System.out.println("No tiene saldo suficiente para realizar esta operación");
                    }
                    break;
                case 3:
                    System.out.println("Su saldo actual es: " + saldo);
                    break;
                case 4:
                    salir = true;
                    break;
                default:
                    System.out.println("Opción incorrecta. Por favor seleccione una opción correcta");
                    break;
            }
        }
    }
}

