import java.util.Scanner;

public class Calcular {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("1 numero: ");
        double num1 = input.nextDouble();

        System.out.print("2 numero: ");
        double num2 = input.nextDouble();

        System.out.print("elegi el operador(+, -, *, /, ^): ");
        char operator = input.next().charAt(0);

        double result;

        if (operator == '+') {
            result = num1 + num2;
            System.out.println(num1 + " + " + num2 + " = " + result);
        } else if (operator == '-') {
            result = num1 - num2;
            System.out.println(num1 + " - " + num2 + " = " + result);
        } else if (operator == '*') {
            result = num1 * num2;
            System.out.println(num1 + " * " + num2 + " = " + result);
        } else if (operator == '/') {
            result = num1 / num2;
            System.out.println(num1 + " / " + num2 + " = " + result);
        } else if (operator == '^') {
            result = Math.pow(num1, num2);
            System.out.println(num1 + " ^ " + num2 + " = " + result);
        } else {
            System.out.println("invalido!");
        }

        input.close();
    }
}