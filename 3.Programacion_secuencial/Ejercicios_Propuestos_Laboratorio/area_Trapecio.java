
//Calcular el área de un trapecio a partir de sus cuatro lados.
import java.util.Scanner;

public class area_Trapecio {
    public static void main(String[] args) {
        Scanner imput = new Scanner(System.in);

        System.out.print("Base Myr: ");
        double lado1 = imput.nextDouble();

        System.out.print("Base Mnr: ");
        double lado2 = imput.nextDouble();

        System.out.print("La alt: ");
        double altura = imput.nextDouble();

        double area = ((lado1 + lado2) * altura) / 2;
        System.out.println("El área del trapecio es: " + area);

    }
}