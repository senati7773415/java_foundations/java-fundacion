
//Calcular el valor de la hipotenusa de un triángulo rectángulo a partir de sus dos catetos
import java.util.Scanner;

public class Hipotenusa {
    public static void main(String[] args) {
        double cateto1 = 5;
        double cateto2 = 5;
        double Hipotenusa = Math.sqrt(Math.pow(cateto1, 2) + Math.pow(cateto2, 2));
        System.out.println("La hipotenusa es: " + Hipotenusa);

    }
}
//