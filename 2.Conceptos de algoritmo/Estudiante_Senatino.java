public class Estudiante_Senatino {
    String Nombre_Apellido;
    String dnd_estudio;
    String carrera;
    String cursoMegusta ;
    
    public Estudiante_Senatino(String nombre, String dnd_estudio, String carrera, String curso) {
        this.Nombre_Apellido = nombre;
        this.dnd_estudio = dnd_estudio;
        this.carrera = carrera;
        this.cursoMegusta  = curso;
    }
    
    public void presentarse() {
        System.out.println("Un saludo cordial Instructor. Mi nombre es " + Nombre_Apellido + ".");
        System.out.println("Soy estudiante de  " + dnd_estudio + ".");
        System.out.println("Estudio la carrera tecnica " + carrera + ".");
        System.out.println("El curso que más me gusta es  " + cursoMegusta + ".");
    }
    
    public static void main(String[] args) {
        Estudiante_Senatino estudiante1 = new Estudiante_Senatino("Wilmer Y. Curro Garate", "Senati", "Ingeniería de software con inteligencia artificial", "JAVA FOUNDATIONS (ORACLE)");
        estudiante1.presentarse();
    }
}
