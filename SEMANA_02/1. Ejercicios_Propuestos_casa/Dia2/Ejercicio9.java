import java.util.Arrays;
public class Ejercicio9 {
    public static void main(String[] args) {
        int[] arr = {4, 2, 1, 6, 5, 3};
        double median = findMedian(arr);
        System.out.println("Median: " + median);
    }
    public static double findMedian(int[] arr) {
        Arrays.sort(arr);

        int n = arr.length;
        double median;
        if (n % 2 == 0) {
            int middle1 = arr[n / 2];
            int middle2 = arr[n / 2 - 1];
            median = (double) (middle1 + middle2) / 2;
        } else {
            median = (double) arr[n / 2];
        }

        return median;
    }
}
