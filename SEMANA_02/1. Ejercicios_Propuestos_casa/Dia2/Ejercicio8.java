import java.util.*;

public class Ejercicio8 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6};
        int count = countPairsWithPrimeSum(arr);
        System.out.println("Number of pairs with prime sum: " + count);
    }
    public static int countPairsWithPrimeSum(int[] arr) {
        int n = arr.length;
        int count = 0;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                int sum = arr[i] + arr[j];
                if (isPrime(sum)) {
                    count++;
                }
            }
        }
        return count;
    }
    public static boolean isPrime(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}
