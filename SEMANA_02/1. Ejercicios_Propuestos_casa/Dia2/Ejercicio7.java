import java.util.Arrays;

public class Ejercicio7 {
    public static void main(String[] args) {
        int[] arreglo = {3, 1, 5, 7, 2, 4, 6};
        int k = 3;
        int kthLargest = encontrarKesimoMasGrande(arreglo, k);
        System.out.println("El " + k + "-ésimo elemento más grande es: " + kthLargest);
    }
    public static int encontrarKesimoMasGrande(int[] arreglo, int k) {
        int n = arreglo.length;
        int left = 0;
        int right = n - 1;
        while (true) {
            int pivotIndex = particionar(arreglo, left, right);
            if (pivotIndex == k - 1) {
                return arreglo[pivotIndex];
            } else if (pivotIndex > k - 1) {
                right = pivotIndex - 1;
            } else {
                left = pivotIndex + 1;
            }
        }
    }
    public static int particionar(int[] arreglo, int left, int right) {
        int pivot = arreglo[right];
        int i = left - 1;
        for (int j = left; j < right; j++) {
            if (arreglo[j] >= pivot) {
                i++;
                intercambiar(arreglo, i, j);
            }
        }
        intercambiar(arreglo, i + 1, right);
        return i + 1;
    }
    public static void intercambiar(int[] arreglo, int i, int j) {
        int temp = arreglo[i];
        arreglo[i] = arreglo[j];
        arreglo[j] = temp;
    }
}
