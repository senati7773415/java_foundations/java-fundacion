public class Ejercicio13 {
    public static void main(String[] args) {
       int[] numeros = {5, 2, 8, 3, 1, 7}; 
       int contadorImpares = 0; 
       for (int i = 0; i < numeros.length; i++) {
          if (numeros[i] % 2 != 0) {
             contadorImpares++;
          }
       }
       System.out.println("Hay " + contadorImpares + " números impares en el arreglo.");
    }
 }
 