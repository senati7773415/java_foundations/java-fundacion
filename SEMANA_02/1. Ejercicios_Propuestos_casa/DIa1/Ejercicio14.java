import java.util.Scanner;
public class Ejercicio14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese un número entero positivo: ");
        int numero = scanner.nextInt();
        scanner.close();

        for (int i = 1; i <= numero; i++) {
            if (i % 2 != 0) {
                System.out.print(i);
                if (i != numero && i != numero - 1) {
                    System.out.print("; ");
                }
            }
        }
    }
}