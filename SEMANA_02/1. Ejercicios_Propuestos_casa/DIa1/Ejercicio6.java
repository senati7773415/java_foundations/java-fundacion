import java.util.Scanner;

public class Ejercicio6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Introduzca la cantidad de números: ");
        int n = input.nextInt();
        int[] numeros = new int[n];
        System.out.println("Introduzca los números:");
        for (int i = 0; i < n; i++) {
            numeros[i] = input.nextInt();
        }
        System.out.println("Números introducidos:");
        imprimir(numeros);
        insercion(numeros);
        System.out.println("Números ordenados:");
        imprimir(numeros);
    }
    
    public static void insercion(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int temp = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > temp) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = temp;
        }
    }
    
    public static void imprimir(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}
