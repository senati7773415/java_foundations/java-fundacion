public class Ejercicio11 {
    public static void main(String[] args) {
        double montoMensual = 0.0;
        double totalPagado = 0.0;
        for (int mes = 1; mes <= 20; mes++) {
            double monto = 10 * Math.pow(2, mes - 1);
            montoMensual += monto;
            totalPagado += monto;
            System.out.println("Mes " + mes + ": S/" + monto);
        }
        System.out.println("Monto mensual: S/" + montoMensual / 20);
        System.out.println("Total pagado: S/" + totalPagado);
    }
}