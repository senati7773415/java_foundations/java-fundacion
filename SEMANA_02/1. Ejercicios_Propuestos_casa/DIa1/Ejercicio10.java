import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Ejercicio10 {
    public static void main(String[] args) {
        // Leer los valores desde el archivo
        ArrayList<Integer> valores = new ArrayList<Integer>();
        try {
            File archivo = new File("valores.txt");
            Scanner scanner = new Scanner(archivo);
            while (scanner.hasNextInt()) {
                valores.add(scanner.nextInt());
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            return;
        }
        // Convertir el arreglo dinámico en un arreglo estático
        int[] arreglo = new int[valores.size()];
        for (int i = 0; i < arreglo.length; i++) {
            arreglo[i] = valores.get(i);
        }
        // Calcular el promedio
        double promedio = Arrays.stream(arreglo).average().orElse(Double.NaN);
        // Calcular la moda
        int moda = 0;
        int maximoFrecuencia = 0;
        for (int i = 0; i < arreglo.length; i++) {
            int frecuencia = Collections.frequency(valores, arreglo[i]);
            if (frecuencia > maximoFrecuencia) {
                moda = arreglo[i];
                maximoFrecuencia = frecuencia;
            }
        }

        // Calcular la desviación 
        double suma = 0.0;
        for (int i = 0; i < arreglo.length; i++) {
            suma += Math.pow(arreglo[i] - promedio, 2);
        }
        double desviacionEstandar = Math.sqrt(suma / arreglo.length);
        System.out.println("Promedio: " + promedio);
        System.out.println("Moda: " + moda);
        System.out.println("Desviación estándar: " + desviacionEstandar);
    }
}