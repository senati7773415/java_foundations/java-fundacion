import java.util.Scanner;
public class Ejercicio1{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduzca una palabra: ");
        String palabra = scanner.nextLine();
        int i = 0;
        while (i < palabra.length()) {
            char letra = palabra.substring(i, i+1).charAt(0);
            System.out.println("Letra " + (i+1) + ": " + letra);
            i++;
        }
    }
}

