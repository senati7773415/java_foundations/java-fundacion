import java.util.Scanner;
public class Ejercicio9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Introduzca el primer término de la serie: ");
        double a = scanner.nextDouble();
        
        System.out.print("Introduzca la razón o factor común de la serie: ");
        double r = scanner.nextDouble();
        
        System.out.print("Introduzca el número de términos de la serie: ");
        int n = scanner.nextInt();
        
        double s = 0;
        for (int i = 0; i < n; i++) {
            s += a * Math.pow(r, i);
        }
        
        System.out.println("La suma de los primeros " + n + " términos de la serie es: " + s);
    }
}
