import java.util.Scanner;

public class Ejercicio7{

    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el número de términos que desea imprimir: ");
        int n = sc.nextInt();
        
        int[] sucesion = new int[n];
        sucesion[0] = 1;
        sucesion[1] = 2;
        
        for (int i = 2; i < n; i++) {
            int contador = 0;
            for (int j = 0; j < i; j++) {
                for (int k = j + 1; k < i; k++) {
                    if (sucesion[j] + sucesion[k] == sucesion[i - 1]) {
                        contador++;
                    }
                }
            }
            if (contador == 1) {
                sucesion[i] = 0;
            } else {
                for (int j = sucesion[i - 1] + 1; j < Integer.MAX_VALUE; j++) {
                    int contador2 = 0;
                    for (int k = 0; k < i; k++) {
                        if (sucesion[k] < j && sucesion[i - 1] != sucesion[k]) {
                            for (int l = k + 1; l < i; l++) {
                                if (sucesion[l] < j && sucesion[i - 1] != sucesion[l] && sucesion[k] + sucesion[l] == j) {
                                    contador2++;
                                }
                            }
                        }
                    }
                    if (contador2 == 1) {
                        sucesion[i] = j;
                        break;
                    }
                }
            }
        }
        
        System.out.println("Los primeros " + n + " términos de la sucesión de Ulam son:");
        for (int i = 0; i < n; i++) {
            System.out.print(sucesion[i] + " ");
        }

    }

}
