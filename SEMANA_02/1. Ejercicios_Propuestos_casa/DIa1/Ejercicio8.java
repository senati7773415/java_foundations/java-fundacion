import java.util.Scanner;

public class Ejercicio8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese un número: ");
        int number = input.nextInt();
        int sum = 0;
        int copy = number;

        // Calculamos la suma de los dígitos
        while (copy > 0) {
            sum += copy % 10;
            copy /= 10;
        }

        // Verificamos si es un número de Harshad
        if (number % sum == 0) {
            System.out.println(number + " es un número de Harshad");
        } else {
            System.out.println(number + " no es un número de Harshad");
        }
    }
}
