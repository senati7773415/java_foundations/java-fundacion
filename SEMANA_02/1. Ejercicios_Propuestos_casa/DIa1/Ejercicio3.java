import java.util.Scanner;
public class Ejercicio3 {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Debe introducir un valor numérico como argumento.");
            return;
        }
        int n = Integer.parseInt(args[0]); // se indica el índice del elemento del arreglo que se desea convertir
        long factorial = calcularFactorial(n);
        System.out.println("El factorial de " + n + " es " + factorial + ".");
    }

    public static long calcularFactorial(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("El valor numérico debe ser no negativo.");
        }
        long factorial = 1;
        for (int i = 1; i <= n; i++) {
            factorial *= i;
        }
        return factorial;
    }
}
