public class Ejercicio15{
    public static void main(String[] args) {
        double prestamo = 5000.0;
        double tasa = 0.016;
        int meses = 18;
        double pago_mensual = prestamo / meses;

        for (int mes = 1; mes <= meses; mes++) {
            double saldo = prestamo * Math.pow(1 + tasa, mes) - pago_mensual * ((Math.pow(1 + tasa, mes) - 1) / tasa);
            System.out.printf("Mes %d: S/%.2f\n", mes, saldo);
        }
    }
}