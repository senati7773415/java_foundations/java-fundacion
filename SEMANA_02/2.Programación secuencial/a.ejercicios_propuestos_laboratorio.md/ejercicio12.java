import java.util.Scanner;

public class ejercicio12{
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print("¿Si esta caendo lluvia? (sí-no): ");
    String lluvia = scanner.nextLine().toLowerCase();

    if (lluvia.equals("sí")) {
      System.out.print("¿Está haciendo mucho viento? (sí-no): ");
      String viento = scanner.nextLine().toLowerCase();

      if (viento.equals("sí")) {
        System.out.println("Hace mucho viento para salir con una paragua.");
      } else {
        System.out.println("Lleva una paragua por si las dudas.");
      }
    } else {
      System.out.println("Que tengas un muy buen día.");
    }
  }
}