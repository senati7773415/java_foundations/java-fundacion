import java.util.Scanner;

public class ejercicio9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("ingrese el número de elementos: ");
        int numElementos = sc.nextInt();
        int[] elementos = new int[numElementos];
        for (int i = 0; i < numElementos; i++) {
            System.out.print("ingrese el elemento " + (i+1) + ": ");
            elementos[i] = sc.nextInt();
        }
        double varianza = calcularVarianza(elementos);
        System.out.println("la varianza de los elementos es: " + varianza);
    }
    public static double calcularVarianza(int[] elementos) {
        double promedio = calcularPromedio(elementos);
        double suma = 0;
        for (int i = 0; i < elementos.length; i++) {
            suma += Math.pow(elementos[i] - promedio, 2);
        }
        return suma / elementos.length;
    }
    public static double calcularPromedio(int[] elementos) {
        int suma = 0;
        for (int i = 0; i < elementos.length; i++) {
            suma += elementos[i];
        }
        return (double) suma / elementos.length;
    }
}