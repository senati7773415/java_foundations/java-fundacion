import java.util.Scanner;

public class ejercicio3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de notas: ");
        int numElementos = sc.nextInt();

        int[] elementos = new int[numElementos];

        for (int i = 0; i < numElementos; i++) {
            System.out.print("Ingrese el elemento " + (i+1) + ": ");
            elementos[i] = sc.nextInt();
        }
        double promedio = calcularPromedio(elementos);
        System.out.println("El promedio de los elementos es: " + promedio);
    }
    public static double calcularPromedio(int[] elementos) {
        int suma = 0;
        for (int i = 0; i < elementos.length; i++) {
            suma += elementos[i];
        }
        return (double) suma / elementos.length;
    }
}