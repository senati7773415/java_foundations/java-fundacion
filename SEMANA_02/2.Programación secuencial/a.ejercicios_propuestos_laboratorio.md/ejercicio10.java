import java.util.Arrays;
public class ejercicio10 {
    public static void main(String[] args) {
        int[] arreglo = {5, 3, 8, 1, 2, 7, 4, 6};
        ordenarDescendente(arreglo);
        System.out.println("El arreglo ordenado en descendente es: " + Arrays.toString(arreglo));
    }

    public static void ordenarDescendente(int[] arreglo) {
        for (int i = 0; i < arreglo.length - 1; i++) {
            for (int j = i + 1; j < arreglo.length; j++) {
                if (arreglo[i] < arreglo[j]) {
                    int temp = arreglo[i];
                    arreglo[i] = arreglo[j];
                    arreglo[j] = temp;
                }
            }
        }
    }
}
