import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Intrduzca el número (entre 1000 y 7000): ");
        int numero = scanner.nextInt();
        int[] digitos = new int[4];
        int indice = 0;
        while (numero > 0) {
            int digito = numero % 10;
            digitos[indice] = digito;
            indice++;
            numero /= 10;
        }
        for (int i = digitos.length - 1; i >= 0; i--) {
            System.out.println("[" + i + "] = " + digitos[i]);
        }
    }
}
