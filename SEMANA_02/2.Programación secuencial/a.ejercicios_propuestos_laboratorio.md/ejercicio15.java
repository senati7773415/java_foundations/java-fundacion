import java.util.Scanner;
public class ejercicio15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número: ");
        int numero = sc.nextInt();
        String numerosRomanos = convertirANumerosRomanos(numero);
        System.out.println("El número " + numero + " en números romanos es: " + numerosRomanos);
    }
    public static String convertirANumerosRomanos(int numero) {
        String[] simbolos = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "VIII","VII","VI" ,"V", "IV", "III","II","i"};
        int[] valores = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 8 , 7 ,6 ,5, 4, 3 , 2 , 1 };
        StringBuilder numerosRomanos = new StringBuilder();
        for (int i = 0; i < simbolos.length; i++) {
            while (numero >= valores[i]) {
                numerosRomanos.append(simbolos[i]);
                numero -= valores[i];
            }
        }
        return numerosRomanos.toString();
    }
}