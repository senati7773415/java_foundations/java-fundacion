public class ejercicio2 {
    public static int maximoEnMatriz(int[][] matriz) {
        int filas = matriz.length;
        int columnas = matriz[0].length;
        
        if (filas == 0 || columnas == 0) {
            return Integer.MIN_VALUE;
        } else if (filas == 1 && columnas == 1) {
            return matriz[0][0];
        } else {
            int mitadFilas = filas / 2;
            int[][] primeraMitad = new int[mitadFilas][columnas];
            int[][] segundaMitad = new int[filas - mitadFilas][columnas];
            
            for (int i = 0; i < mitadFilas; i++) {
                primeraMitad[i] = matriz[i];
            }
            
            for (int i = mitadFilas; i < filas; i++) {
                segundaMitad[i - mitadFilas] = matriz[i];
            }
            
            int maximoPrimeraMitad = maximoEnMatriz(primeraMitad);
            int maximoSegundaMitad = maximoEnMatriz(segundaMitad);
            
            return Math.max(maximoPrimeraMitad, maximoSegundaMitad);
        }
    }
    
    public static void main(String[] args) {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int maximo = maximoEnMatriz(matriz);
        System.out.println("El máximo valor en la matriz es: " + maximo);
    }
}
