public class ejercicio4 {
    public static void imprimirInverso(int n) {
        if (n == 0) {
            return;
        } else {
            System.out.print(n + " ");
            imprimirInverso(n - 1);
        }
    }
    
    public static void main(String[] args) {
        int n = 10;
        System.out.println("Los números del 1 al " + n + " en orden inverso son:");
        imprimirInverso(n);
    }
}
