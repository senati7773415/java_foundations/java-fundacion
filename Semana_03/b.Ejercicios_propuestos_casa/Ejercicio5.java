public class ejercicio5 {
    public static void dibujarGrafico(int n, int m) {
        if (n == 7) {
            return;
        } else {
            for (int i = 0; i < 7 - n; i++) {
                System.out.print(" ");
            }
            for (int i = 0; i < m; i++) {
                System.out.print("*");
            }
            System.out.println();
            dibujarGrafico(n + 1, m + 1);
        }
    }
    
    public static void main(String[] args) {
        dibujarGrafico(0, 1);
    }
}
