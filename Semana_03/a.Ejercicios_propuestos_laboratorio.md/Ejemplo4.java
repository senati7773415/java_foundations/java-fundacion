import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Ejemplo4 {

    public static void leerArchivo(String archivo) {
        try (BufferedReader br = new BufferedReader(new FileReader(archivo))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                System.out.println(linea);
            }
        } catch (IOException e) {
            System.out.println("Error al leer el archivo " + archivo);
        }
    }

    public static void main(String[] args) {
        String archivo = "aleatorios.txt";
        leerArchivo(archivo);
    }
}
