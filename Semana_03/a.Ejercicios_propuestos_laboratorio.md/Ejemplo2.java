//Ejemplo de lectura y escritura de archivos en java
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Ejemplo2{

    public static void main(String[] args) {
        
        // Abre el archivo para leer
        File archivoLectura = new File("archivo.txt");
        Scanner lector;
        try {
            lector = new Scanner(archivoLectura);
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            return;
        }
        
        // Lee el contenido del archivo
        while (lector.hasNextLine()) {
            String linea = lector.nextLine();
            System.out.println(linea);
        }
        lector.close();
        
        // Abre el archivo para escribir
        File archivoEscritura = new File("archivo.txt");
        PrintWriter escritor;
        try {
            escritor = new PrintWriter(archivoEscritura);
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            return;
        }
        
        // Escribe en el archivo
        escritor.println("¡Los de SENATI son unos tigres en programación!");
        escritor.println("Este es un ejemplo de escritura en un archivo de texto.");
        escritor.close();
        
        System.out.println("Archivo actualizado.");
    }

}

