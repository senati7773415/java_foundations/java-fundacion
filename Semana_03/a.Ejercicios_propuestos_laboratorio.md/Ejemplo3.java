import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Ejemplo3{
    private static final int LIMITE_SUPERIOR = 300; //Límite superior del rango de los aleatorios
    private static final String NOMBRE_ARCHIVO = "aleatorios.txt"; //Nombre del archivo de texto

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);

        System.out.print("Indique, ¿cuántos números se generarán?: ");
        int n = leer.nextInt();

        int[] aleatorios = generarAleatorios(n, LIMITE_SUPERIOR);
        guardarEnArchivo(aleatorios, NOMBRE_ARCHIVO);
        leer.close();
    }
    
    //Método para generar n números aleatorios recursivamente
    public static int[] generarAleatorios(int n, int maximo) {
        int[] aleatorios = new int[n];
        generarAleatoriosRecursivo(n, maximo, aleatorios, 0);
        return aleatorios;
    }
    
    private static void generarAleatoriosRecursivo(int n, int maximo, int[] aleatorios, int indice) {
        if (n == 0) {
            return;
        }
        Random random = new Random();
        int aleatorio = random.nextInt(maximo);
        aleatorios[indice] = aleatorio;
        generarAleatoriosRecursivo(n - 1, maximo, aleatorios, indice + 1);
    }
    
    //Método para guardar un arreglo de enteros en un archivo de texto
    public static void guardarEnArchivo(int[] numeros, String nombreArchivo) {
        try {
            FileWriter escritor = new FileWriter(nombreArchivo);
            for (int numero : numeros) {
                escritor.write(numero + " ");
            }
            escritor.close();
            System.out.println("Los números se han guardado en el archivo " + nombreArchivo + ".");
        } catch (IOException e) {
            System.out.println("Error al guardar en archivo.");
            e.printStackTrace();
        }
    }

}

