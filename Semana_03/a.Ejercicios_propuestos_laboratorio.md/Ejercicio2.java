public class Ejercicio2 {
    public static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }

    public static int gcd(int[] numbers) {
        int result = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            result = gcd(result, numbers[i]);
        }
        return result;
    }

    public static void main(String[] args) {
        int[] numbers = { 10, 15, 20, 25 };
        int result = gcd(numbers);
        System.out.println("El máximo común divisor de los números es: " + result);
    }
}
