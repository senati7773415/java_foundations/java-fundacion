/*import java.io.*;
import jxl.*;

public class Ejercicio8 {
    private String nombreCompleto;
    private String grupoPersonas;
    
    public Persona(String nombreCompleto, String grupoPersonas) {
        this.nombreCompleto = nombreCompleto;
        this.grupoPersonas = grupoPersonas;
    }
    
    public String getNombreCompleto() {
        return nombreCompleto;
    }
    
    public String getGrupoPersonas() {
        return grupoPersonas;
    }
    
    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }
    
    public void setGrupoPersonas(String grupoPersonas) {
        this.grupoPersonas = grupoPersonas;
    }
}

public class Trabajador extends Persona {
    public Trabajador(String nombreCompleto, String grupoPersonas) {
        super(nombreCompleto, grupoPersonas);
    }
}

public static void main(String[] args) {
    // Creamos un arreglo para almacenar los trabajadores
    Trabajador[] trabajadores = new Trabajador[10]; // Supongamos que hay 10 trabajadores
    
    try {
        // Abrimos el archivo de Excel y obtenemos la primera hoja
        Workbook workbook = Workbook.getWorkbook(new File("clientes.xls"));
        Sheet sheet = workbook.getSheet(0);
        
        // Recorremos todas las filas de la hoja, excepto la primera que contiene los encabezados
        for (int fila = 1; fila < sheet.getRows(); fila++) {
            // Obtenemos los valores de las columnas nombre completo y grupo de personas
            String nombreCompleto = sheet.getCell(0, fila).getContents();
            String grupoPersonas = sheet.getCell(1, fila).getContents();
            
            // Creamos un objeto trabajador y lo agregamos al arreglo
            Trabajador trabajador = new Trabajador(nombreCompleto, grupoPersonas);
            trabajadores[fila-1] = trabajador;
        }
        
        // Ordenamos el arreglo de trabajadores por nombre completo
        Arrays.sort(trabajadores, new Comparator<Trabajador>() {
            @Override
            public int Comparator(Trabajador t1, Trabajador t2) {
                return t1.getNombreCompleto().compareTo(t2.getNombreCompleto());
            }
        });
        
        // Guardamos los datos procesados en el archivo datos_procesados.txt
        PrintWriter writer = new PrintWriter("datos_procesados.txt");
        for (Trabajador trabajador : trabajadores) {
            writer.println(trabajador.getNombreCompleto() + "," + trabajador.getGrupoPersonas());
        }
        writer.close();
        
    } catch (Exception e) {
        e.printStackTrace();
    }
}
