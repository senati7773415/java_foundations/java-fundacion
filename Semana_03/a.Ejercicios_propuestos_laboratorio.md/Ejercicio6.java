import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Ejercicio6 {
    public static void main(String[] args) {
        // Generar los 1000 datos aleatorios y guardarlos en un archivo
        try {
            FileWriter writer = new FileWriter("datos_generados.txt");
            for (int i = 0; i < 1000; i++) {
                int dato = (int) (Math.random() * 301);
                writer.write(dato + "\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Leer los datos del archivo y calcular las estadísticas
        ArrayList<Integer> datos = new ArrayList<Integer>();
        try {
            FileReader reader = new FileReader("datos_generados.txt");
            Scanner scanner = new Scanner(reader);
            while (scanner.hasNextLine()) {
                int dato = Integer.parseInt(scanner.nextLine());
                datos.add(dato);
            }
            scanner.close();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int valor = 0;
        int maximo = Collections.max(datos);
        int minimo = Collections.min(datos);
        double promedio = datos.stream().mapToInt(Integer::intValue).average().orElse(0.0);
        int moda = calcularModa(datos);
        double desviacion = calcularDesviacion(datos);

        // Guardar los resultados en un archivo
        try {
            FileWriter writer = new FileWriter("resultados.txt");
            writer.write("Valor: " + valor + "\n");
            writer.write("Máximo: " + maximo + "\n");
            writer.write("Mínimo: " + minimo + "\n");
            writer.write("Promedio: " + promedio + "\n");
            writer.write("Moda: " + moda + "\n");
            writer.write("Desviación estándar: " + desviacion + "\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static int calcularModa(ArrayList<Integer> datos) {
        Map<Integer, Integer> frecuencias = new HashMap<Integer, Integer>();
        for (int dato : datos) {
            if (frecuencias.containsKey(dato)) {
                frecuencias.put(dato, frecuencias.get(dato) + 1);
            } else {
                frecuencias.put(dato, 1);
            }
        }
        int moda = 0;
        int maxFrecuencia = 0;
        for (Map.Entry<Integer, Integer> entry : frecuencias.entrySet()) {
            if (entry.getValue() > maxFrecuencia) {
                moda = entry.getKey();
                maxFrecuencia = entry.getValue();
            }
        }
        return moda;
    }
    public static double calcularDesviacion(ArrayList<Integer> datos) {
        double promedio = datos.stream().mapToInt(Integer::intValue).average().orElse(0.0);
        double sumatoria = 0;
        for (int dato : datos) {
            sumatoria += Math.pow(dato - promedio, 2);
        }
        double varianza = sumatoria / datos.size();
        double desviacion = Math.sqrt(varianza  );
        return desviacion;
    }
}