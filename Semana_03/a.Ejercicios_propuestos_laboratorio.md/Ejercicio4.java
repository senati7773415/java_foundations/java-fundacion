import java.util.Scanner;

public class Ejercicio4 {
    public static int factorial(int n) {
        if (n == 0) {
            return 1;
        }
        return n * factorial(n - 1);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Intrduzca la cantidad de elementos: ");
        int n = scanner.nextInt();
        scanner.close();
        int resultado = factorial(n);
        System.out.println("El número de permutaciones de los " + n + " elementos es: " + resultado);
    }
}
